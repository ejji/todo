import { Component, OnInit } from '@angular/core';
//import {DragulaService, Dragula} from 'ng2-dragula/ng2-dragula';
import {TaskBoardComponent} from '../task-board/task-board.component.ts';
import {TasksService} from '../task-board/tasks.service.ts';



@Component({
  selector: 'my-home',
  template: require('./home.component.html'),
  styles: [require('./home.component.scss')],
  directives: [
    TaskBoardComponent
  ],
  //viewProviders: [DragulaService],
  providers: [TasksService]
})
export class HomeComponent implements OnInit {
  private tasks;
  private users;
  private curentUser;
  private taskBoardData = { tasks: {}, users: {}, curentUser: {} };

  constructor(private taskServive: TasksService) {

  }

  ngOnInit() {
    console.log('Hello Home');
    this.taskBoardData.tasks = this.taskServive.getTasks();
    this.taskBoardData.users = this.taskServive.getUsers();
    this.taskBoardData.curentUser = this.taskServive.getСurentUser();

    console.log(this.taskServive.getTasks());
  }

  tasksUpdated(tasks) {
    console.log(tasks);
    // this.taskServive.updateTasks(tasks);
    this.tasks = tasks;
  }

}
