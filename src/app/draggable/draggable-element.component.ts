import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { ElementRef, Renderer } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'draggable-element',
    template: require('./draggable-element.component.html'),
    styles: [require('./draggable-element.component.css')],
    directives: [],
    pipes: []
    //,
    //viewProviders: [DragulaService]
})
export class DraggableElementComponent implements OnInit {
    // @Input() tasks;
    // @Input() users;  

    @Input() test;

    @Input() data;
    @Output() onDrag = new EventEmitter();
    @Output() onDop = new EventEmitter();

    constructor(elementRef: ElementRef, renderer: Renderer) { 
        var rootEl = elementRef.nativeElement;        
        rootEl.setAttribute('draggable', true);
        //console.log(rootEl);      
    };

    ngOnInit() {   
             
    };
}
