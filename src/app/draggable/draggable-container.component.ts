import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { ElementRef, Renderer } from '@angular/core';

import { Observable } from 'rxjs/Rx';



@Component({
    selector: 'draggable-container',
    template: require('./draggable-container.component.html'),
    styles: [require('./draggable-container.component.css')],
    directives: [],
    pipes: []
    //,
    //viewProviders: [DragulaService]
})
export class DraggableContainerComponent implements OnInit {
    // @Input() tasks;
    // @Input() users;
    @Input() options;
    @Output() onContainerUpdated = new EventEmitter();
    private elementRef: ElementRef;
    private renderer: Renderer;

    constructor(elementRef: ElementRef, renderer: Renderer) {
        this.elementRef = elementRef;
        this.renderer = renderer;

    };

    ngOnInit() {
        this.sortable(this.elementRef.nativeElement, this.renderer, this.options, this.onContainerUpdated);
    };

    sortable(rootEl, renderer, options, callback) {
        //var self = this;
        //var rootEl = ;
        //console.log(rootEl);
        console.log(options.useMultiContainers);

        //on drag start
        renderer.listen(rootEl, 'dragstart', (e) => {
            console.log('drag');

            var dragEl = e.target;

            var fromContainer = dragEl.parentElement;
            var movedElementIndex;

            //console.log(fromContainer);

            if (options.useMultiContainers) {
                var parentNodeList = [].slice.call(rootEl.getElementsByClassName('subContainer'));
                parentNodeList.forEach(container => {
                    var sortableList = [].slice.call(container.getElementsByTagName('draggable-element'));
                    sortableList.forEach(sortable => {
                        sortable.positionInSortable = sortableList.indexOf(sortable);
                        //console.log(sortable.positionInSortable);
                        if (sortableList.indexOf(e.target) != -1) {
                            movedElementIndex = sortableList.indexOf(e.target);
                        }
                    });
                });

            } else {
                var nodeList = [].slice.call(rootEl.getElementsByTagName('draggable-element'));
                nodeList.forEach(e => {
                    e.positionInSortable = nodeList.indexOf(e);
                });
            }

            function onDragOver(e) {
                //console.log('dragOver');

                e.preventDefault();
                e.dataTransfer.dropEffect = 'move';

                var target = e.target;
                //if (target.hasAttribute("draggable")) {
                //console.log(target);
                // }

                if (target && target !== dragEl && target.hasAttribute("draggable")) {
                    //if (target && target !== dragEl && target.nodeName == 'draggable-element') {
                    // Сортируем
                    var rect = target.getBoundingClientRect();
                    var next = (e.clientY - rect.top) / (rect.bottom - rect.top) > .5;
                    //console.log(target.parentNode.nextSibling);

                    //rootEl.insertBefore(dragEl, next && target.nextSibling || target);
                    target.parentElement.insertBefore(dragEl, next && target.nextSibling || target);

                    //console.log('dragOver');
                    //console.log(e.target);
                    dragEl.classList.add('ghost');
                }
            }

            // Окончание сортировки
            function onDragEnd(e) {
                e.preventDefault();

                var toContainer = e.target.parentElement;

                dragEl.classList.remove('ghost');
                rootEl.removeEventListener('dragover', onDragOver, false);
                rootEl.removeEventListener('dragend', onDragEnd, false);

                //if( nextEl !== dragEl.nextSibling ){
                // Сообщаем об окончании сортировки
                //onUpdate(dragEl);
                //}

                if (options.useMultiContainers) {
                    var positionListFrom = [].slice.call(fromContainer.getElementsByTagName('draggable-element'));
                    var containerValFrom = fromContainer.getAttribute('returnVal');
                    positionListFrom = positionListFrom.map(e => { return e.positionInSortable });
                    //console.log(fromContainer.getAttribute('returnVal'));

                    var positionListTo = [].slice.call(toContainer.getElementsByTagName('draggable-element'));
                    var containerValTo = toContainer.getAttribute('returnVal');
                    positionListTo = positionListTo.map(e => { return e.positionInSortable });
                    //console.log(toContainer.getAttribute('returnVal'));


                    console.log(positionListFrom);
                    callback.emit({ positionListFrom, positionListTo, containerValFrom, containerValTo, movedElementIndex });
                    //callback.emit("abc");

                } else {
                    var nodeListNew = [].slice.call(rootEl.getElementsByTagName('draggable-element'));
                    // nodeListNew.forEach(e => {
                    //     console.log(e.positionInSortable)
                    // });
                    nodeListNew = nodeListNew.map(e => { return e.positionInSortable });
                }
                //console.log(nodeListNew);


                //this.onContainerUpdated.emit({ positionList: nodeListNew });

            }
            // Ограничиваем тип перетаскивания
            e.dataTransfer.effectAllowed = 'move';
            e.dataTransfer.setData('Text', dragEl.textContent);

            //rootEl.setAttribute('draggable', true);
            rootEl.addEventListener('dragover', onDragOver, false);
            rootEl.addEventListener('dragend', onDragEnd, false);
        });

    };


}