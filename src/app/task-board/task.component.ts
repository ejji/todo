import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';


@Component({
    selector: 'my-task',
    template: `
        <span 
            [attr.taskId]="task.taskId" 
            [attr.userId]="task.userId" 
            [attr.prevTaskId]="task.prevTaskId"
        >
            {{task.taskStatus}}:{{task.taskId}}:{{task.taskName}}
            </span><span (click)="onDelete()">delete</span>
             <span (click)="onChange()">edit
        </span>
    `
})
export class TaskComponent implements OnInit {
    @Input() task;
    @Output('onDelete') onDeleteEm = new EventEmitter();
    @Output('onChange') onChangeEm = new EventEmitter();
    constructor() { }

    ngOnInit() {
        // console.log('TaskComponent ngOnInit()');
    }

    onDelete() {
        this.onDeleteEm.emit({ taskId: this.task.taskId });
    }

    onChange() {
        console.log(this.task.taskName);
        this.onChangeEm.emit({ taskId: this.task.taskId, taskName: this.task.taskName });
    }

}
