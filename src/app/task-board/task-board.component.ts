import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { TaskComponent } from './task.component';
import { FilterPipe } from '../shared/filter.pipe';
//import {DragulaService, Dragula} from 'ng2-dragula/ng2-dragula';
import { DraggableContainerComponent } from '../draggable/draggable-container.component.ts';
import { DraggableElementComponent } from '../draggable/draggable-element.component.ts';


@Component({
    selector: 'my-task-board',
    template: require('./task-board.component.html'),
    styles: [require('./task-board.component.css')],
    directives: [
        TaskComponent,
        DraggableContainerComponent,
        DraggableElementComponent
    ],
    pipes: [FilterPipe]
    //,
    //viewProviders: [DragulaService]
})
export class TaskBoardComponent implements OnInit {
    // @Input() tasks;
    // @Input() users;
    @Input() data;
    @Output() onTasksUpdated = new EventEmitter();
    private tasks;
    private users;
    private editedTask = { taskName: '', taskId: null };
    private curentUser = {};
    private taskById = [];
    private posSwaper = {};
    // private taskIndexToUserId = [];
    // constructor(private elementRef: ElementRef) { };

    constructor() {
    };

    taskNameChange(e) {
        // console.log(e.taskName);
        this.editedTask = e;
    };

    saveTaskName(e) {
        // console.log(this.editedTask.taskName);
        if (this.editedTask.taskId != null) {
            this.tasks.getTaskById(this.editedTask.taskId).taskName = this.editedTask.taskName;
        }
    };

    ngOnInit() {
        // console.log('TaskBoardComponent ngOnInit()');
        this.tasks = this.data.tasks;
        this.users = this.data.users;
        this.curentUser = this.data.curentUser;

        this.tasks.getTaskById = taskId => {
            //console.log(taskId);  
            if (taskId === false) {
                return -1;
            } else {
                return this.tasks.filter(x => x.taskId === taskId)[0];
            }
        };
    };

    private onDrop(args) {
        let [e, el] = args;
        // this.removeClass(e, 'ex-moved');
        console.log(args);
    }

  debug(e) {
    console.log(e);
    this.tasks = [
        { taskId: 0, userId: 1, prevTaskId: -1, taskName: 'Blah', taskStatus: 0 }       
    ];
  }

}

