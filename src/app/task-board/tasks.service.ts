import { Injectable } from '@angular/core';



@Injectable()
export class TasksService {

    private dummyTasks = [
        { taskId: 0, userId: 1, prevTaskId: -1, taskName: 'Blah', taskStatus: 0 },
        { taskId: 1, userId: 1, prevTaskId: 0, taskName: 'Hah', taskStatus: 0 },
        { taskId: 2, userId: 2, prevTaskId: -1, taskName: 'Vah', taskStatus: 0 },
        { taskId: 3, userId: 1, prevTaskId: 1, taskName: 'Vlah', taskStatus: 0 },
        { taskId: 4, userId: 1, prevTaskId: 1, taskName: 'Clah', taskStatus: 1 },
        { taskId: 5, userId: 1, prevTaskId: 1, taskName: 'Hlah', taskStatus: 2 }
    ];

    private dummyUsers = [
        { userId: 1, userName: 'Steeve' },
        { userId: 2, userName: 'Jobs' }
    ];

    private curentUser = {curentUserId: 1};

    constructor() { }

    getTasks() {
        return this.dummyTasks;
    }

    updateTasks(d) {
        this.dummyTasks = d;
    }

    getUsers() {
        return this.dummyUsers;
    }

    getСurentUser() {
        return this.curentUser;
    }

}
