import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filterF'
})

export class FilterFPipe implements PipeTransform {
    transform(value: any, args: any[]): any {
        let func = new Function('x', 'return '+ args[0]);        
        return value.filter(func.bind(value));        
    }
}